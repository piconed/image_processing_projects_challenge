import numpy as np
import skimage
from tqdm import tqdm

from src.loading import load_patches, save_image


def cutting (img):
    #entry: image 1024x1024 pixels
    #output: a matrix containing all the queries
    mat = []
    for j in range (0,32):
        for i in range (0,32):
            mat.append(img[i*32:i*32+32,j*32:j*32+32])
    return mat


def calculate_psnr(original_image, compressed_image):
    # Convert the RGB images to Grey
    img_original_gray  = skimage.color.rgb2gray(original_image)
    img_compressed_gray = skimage.color.rgb2gray(compressed_image)

    # Calculate the difference between the images
    diff = img_original_gray.astype(np.float64) - img_compressed_gray.astype(np.float64)

    # Calculate the mean square error (MSE)
    mse = np.mean(diff ** 2)

    # Calculating the PSNR
    if mse == 0:
        psnr = float('inf')
    else:
        max_pixel_value = 255  # Valeur maximale pour un pixel de 8 bits
        psnr = 10 * np.log10((max_pixel_value ** 2) / mse)

    return psnr


def comparaison_psnr(image, patches):
    best = patches[0]
    psnr = calculate_psnr(image,best)
    for k in range (0,len(patches)):
        if calculate_psnr(image, patches[k]) > psnr :
            best = patches[k]
            psnr = calculate_psnr(image,best)
    return best , psnr


def comparaison_all(mat, patches):
    final =[]
    liste_psnr = []
    best = patches[0]
    psnr = 0
    for k in tqdm(range (0,len(mat))):
        best,psnr = comparaison_psnr(mat[k],patches)
        final.append(best)
        liste_psnr.append(psnr)
    return final , liste_psnr


def rebuild (final, image_path):
    imgf = skimage.io.imread(image_path)
    for j in range (0,32):
        for i in range (0,32):
            imgf[i*32:i*32+32,j*32:j*32+32] = final[i+j*32]
    return imgf


def run_reconstruction(
        image_path: str,
        patches_path: str,
        output_path: str,
        n_patches: int = None,
) -> None:
    img = skimage.io.imread(image_path)
    patches = load_patches(patches_path, n_patches)
    mat = cutting(img)
    final , liste_psnr =comparaison_all(mat, patches)
    imgf = rebuild (final, image_path)
    save_image(imgf, output_path)
