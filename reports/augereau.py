import numpy as np
from tqdm import tqdm

from src.loading import load_image, save_image


def average_color(image):
    Nimage = image.shape[0]

    mcolor = np.asarray(
        [0, 0, 0])  # Creating an array to store the average color

    for i in range(Nimage):
        for j in range(Nimage):
            mcolor += image[i, j]  # Sum of the color of each pixel

    mcolor = mcolor / (Nimage ** 2)  # Divided by the number of pixels
    return (mcolor)


def best_mse(patch_list, color_subimage, data):
    best_norm = np.inf  # Initialising norm and position to compare later
    best = -1
    for i in range(len(patch_list)):
        patch = data[(int)(patch_list[i])]  # Retrieve the patch from data using the position stored in patch_list
        color_patch = average_color(
            patch)  # Calculate the averae color of the current path
        norm = np.linalg.norm(
            color_subimage - color_patch)  # Computes the L2 norm difference (=MSE) to get the most accurate color
        if (
                norm < best_norm):  # A better candidate has been found, updating norm and position
            best = i
            best_norm = norm
    if best == -1:
        print("Error, list isn't supposed to be empty")
        new_list = []
    else:
        new_list = patch_list.copy()
        new_list.remove(
            patch_list[best])  # Slicing the list to remove the best element
    return (best, new_list)


def run_reconstruction(
        image_path: str,
        patches_path: str,
        output_path: str,
        n_patches: int = None,
) -> None:

    use_mse = False
    precision = 26 # Decreasing might improve the precision of the patch used, but increses computing time and space and chance to have missing patches

    image = load_image(image_path)
    data = np.load(patches_path)
    n_patches = len(data) if n_patches is None else n_patches
    data = data[:n_patches]

    num_images = n_patches
    patch_color = np.zeros((num_images, 3))
    for i in range(num_images):
        patch_color[i, :] = average_color(data[i])

    tensorsize = (int)((260 // precision) + 1)
    tensor = [[[[] for _ in range(tensorsize)] for _ in range(tensorsize)]
              for _ in range(tensorsize)]

    for i in range(num_images):
        xpos = (int)(patch_color[i, 0] // precision)
        ypos = (int)(patch_color[i, 1] // precision)
        zpos = (int)(patch_color[i, 2] // precision)
        tensor[xpos][ypos][zpos].append((int)(i))


    NsI = 32
    missing = 0
    res_num = np.zeros((NsI, NsI), dtype=int)  # Create a matrix in which we wwill store the best positions
    for i in tqdm(range(NsI)):
        for j in range(NsI):
            imgcol = average_color(image[i * 32:(i + 1) * 32, j * 32:(j + 1) * 32])  # Compute the average color of a 32 per 32 section of the image, no overlap
            posx = (int)(imgcol[0] // precision)  # Ideal position to look for patches
            posy = (int)(imgcol[1] // precision)
            posz = (int)(imgcol[2] // precision)
            candidate_list = tensor[posx][posy][posz]
            if (len(candidate_list) > 0):
                if use_mse:
                    res_num[i, j], tensor[posx][posy][posz] = best_mse(candidate_list, imgcol, data)  # Computes the L2 norm difference between each candidate and the subimage
                else:
                    res_num[i, j], tensor[posx][posy][posz] = candidate_list[-1], candidate_list[:-1]  # Retrieves the last element of the list (not optimal)

            # Case : no patch available remaining, search around
            else:
                found = False
                if (posx < tensorsize - 1):
                    posx += 1
                    candidate_list = tensor[posx][posy][posz]
                    if (len(candidate_list) > 0):
                        if use_mse:
                            res_num[i, j], tensor[posx][posy][posz] = best_mse(candidate_list, imgcol, data)
                        else:
                            res_num[i, j], tensor[posx][posy][posz] = candidate_list[-1], candidate_list[:-1]
                        found = True
                if (posx > 0 and not found):
                    posx -= 2
                    candidate_list = tensor[posx][posy][posz]
                    if (len(candidate_list) > 0):
                        if use_mse:
                            res_num[i, j], tensor[posx][posy][posz] = best_mse(candidate_list, imgcol, data)
                        else:
                            res_num[i, j], tensor[posx][posy][posz] = candidate_list[-1], candidate_list[:-1]
                        found = True
                    posx += 1
                if (posy < tensorsize - 1 and not found):
                    posy += 1
                    candidate_list = tensor[posx][posy][posz]
                    if (len(candidate_list) > 0):
                        if use_mse:
                            res_num[i, j], tensor[posx][posy][posz] = best_mse(candidate_list, imgcol, data)
                        else:
                            res_num[i, j], tensor[posx][posy][posz] = candidate_list[-1], candidate_list[:-1]
                        found = True
                if (posy > 0 and not found):
                    posy -= 2
                    candidate_list = tensor[posx][posy][posz]
                    if (len(candidate_list) > 0):
                        if use_mse:
                            res_num[i, j], tensor[posx][posy][posz] = best_mse(
                                candidate_list, imgcol, data)
                        else:
                            res_num[i, j], tensor[posx][posy][posz] = candidate_list[-1], candidate_list[:-1]
                        found = True
                    posy += 1
                if (posz < tensorsize - 1 and not found):
                    posz += 1
                    candidate_list = tensor[posx][posy][posz]
                    if (len(candidate_list) > 0):
                        if use_mse:
                            res_num[i, j], tensor[posx][posy][posz] = best_mse(
                                candidate_list, imgcol, data)
                        else:
                            res_num[i, j], tensor[posx][posy][posz] = candidate_list[-1], candidate_list[:-1]
                        found = True
                if (posz > 0 and not found):
                    posz -= 2
                    candidate_list = tensor[posx][posy][posz]
                    if (len(candidate_list) > 0):
                        if use_mse:
                            res_num[i, j], tensor[posx][posy][posz] = best_mse(candidate_list, imgcol, data)
                        else:
                            res_num[i, j], tensor[posx][posy][posz] = candidate_list[-1], candidate_list[:-1]
                        found = True
                    posz += 1
                # If no 1st order neighbour search in diagonals
                if (not found):
                    if ((posx < tensorsize - 1) and (posy < tensorsize - 1)):
                        posx += 1
                        posy += 1
                        candidate_list = tensor[posx][posy][posz]
                        if (len(candidate_list) > 0):
                            if use_mse:
                                res_num[i, j], tensor[posx][posy][
                                    posz] = best_mse(candidate_list, imgcol,
                                                     data)
                            else:
                                res_num[i, j], tensor[posx][posy][posz] = candidate_list[-1], candidate_list[:-1]
                            found = True
                        posx -= 1
                        posy -= 1
                    if ((posx < tensorsize - 1) and (
                            posz < tensorsize - 1) and not found):
                        posx += 1
                        posz += 1
                        candidate_list = tensor[posx][posy][posz]
                        if (len(candidate_list) > 0):
                            if use_mse:
                                res_num[i, j], tensor[posx][posy][
                                    posz] = best_mse(candidate_list, imgcol,
                                                     data)
                            else:
                                res_num[i, j], tensor[posx][posy][posz] = candidate_list[-1], candidate_list[:-1]
                            found = True
                        posx -= 1
                        posz -= 1
                    if ((posy < tensorsize - 1) and (
                            posz < tensorsize - 1) and not found):
                        posy += 1
                        posz += 1
                        candidate_list = tensor[posx][posy][posz]
                        if (len(candidate_list) > 0):
                            if use_mse:
                                res_num[i, j], tensor[posx][posy][
                                    posz] = best_mse(candidate_list, imgcol,
                                                     data)
                            else:
                                res_num[i, j], tensor[posx][posy][posz] = candidate_list[-1], candidate_list[:-1]
                            found = True
                        posy -= 1
                        posz -= 1
                    if ((posx > 0) and (posy < tensorsize - 1) and not found):
                        posx -= 1
                        posy += 1
                        candidate_list = tensor[posx][posy][posz]
                        if (len(candidate_list) > 0):
                            if use_mse:
                                res_num[i, j], tensor[posx][posy][
                                    posz] = best_mse(candidate_list, imgcol, data)
                            else:
                                res_num[i, j], tensor[posx][posy][posz] = candidate_list[-1], candidate_list[:-1]
                            found = True
                        posx += 1
                        posy -= 1
                    if ((posx > 0) and (posz < tensorsize - 1) and not found):
                        posx -= 1
                        posz += 1
                        candidate_list = tensor[posx][posy][posz]
                        if (len(candidate_list) > 0):
                            if use_mse:
                                res_num[i, j], tensor[posx][posy][
                                    posz] = best_mse(candidate_list, imgcol, data)
                            else:
                                res_num[i, j], tensor[posx][posy][posz] = candidate_list[-1], candidate_list[:-1]
                            found = True
                        posx += 1
                        posz -= 1
                    if ((posy > 0) and (posx < tensorsize - 1) and not found):
                        posy -= 1
                        posx += 1
                        candidate_list = tensor[posx][posy][posz]
                        if (len(candidate_list) > 0):
                            if use_mse:
                                res_num[i, j], tensor[posx][posy][
                                    posz] = best_mse(candidate_list, imgcol,
                                                     data)
                            else:
                                res_num[i, j], tensor[posx][posy][posz] = candidate_list[-1], candidate_list[:-1]
                            found = True
                        posy += 1
                        posx -= 1
                    if ((posy > 0) and (posz < tensorsize - 1) and not found):
                        posy -= 1
                        posz += 1
                        candidate_list = tensor[posx][posy][posz]
                        if (len(candidate_list) > 0):
                            if use_mse:
                                res_num[i, j], tensor[posx][posy][
                                    posz] = best_mse(candidate_list, imgcol, data)
                            else:
                                res_num[i, j], tensor[posx][posy][posz] = candidate_list[-1], candidate_list[:-1]
                            found = True
                        posy += 1
                        posz -= 1
                    if ((posz > 0) and (posx < tensorsize - 1) and not found):
                        posz -= 1
                        posx += 1
                        candidate_list = tensor[posx][posy][posz]
                        if (len(candidate_list) > 0):
                            if use_mse:
                                res_num[i, j], tensor[posx][posy][
                                    posz] = best_mse(candidate_list, imgcol,
                                                     data)
                            else:
                                res_num[i, j], tensor[posx][posy][posz] = candidate_list[-1], candidate_list[:-1]
                            found = True
                        posz += 1
                        posx -= 1
                    if ((posz > 0) and (posy < tensorsize - 1) and not found):
                        posz -= 1
                        posy += 1
                        candidate_list = tensor[posx][posy][posz]
                        if (len(candidate_list) > 0):
                            if use_mse:
                                res_num[i, j], tensor[posx][posy][posz] = best_mse(candidate_list, imgcol, data)
                            else:
                                res_num[i, j], tensor[posx][posy][posz] = candidate_list[-1], candidate_list[:-1]
                            found = True
                        posz += 1
                        posy -= 1
                    if ((posx > 0) and (posy > 0) and not found):
                        posx -= 1
                        posy -= 1
                        candidate_list = tensor[posx][posy][posz]
                        if (len(candidate_list) > 0):
                            if use_mse:
                                res_num[i, j], tensor[posx][posy][
                                    posz] = best_mse(candidate_list, imgcol, data)
                            else:
                                res_num[i, j], tensor[posx][posy][posz] = candidate_list[-1], candidate_list[:-1]
                            found = True
                        posx += 1
                        posy += 1
                    if ((posx > 0) and (posz > 0) and not found):
                        posx -= 1
                        posz -= 1
                        candidate_list = tensor[posx][posy][posz]
                        if (len(candidate_list) > 0):
                            if use_mse:
                                res_num[i, j], tensor[posx][posy][
                                    posz] = best_mse(candidate_list, imgcol, data)
                            else:
                                res_num[i, j], tensor[posx][posy][posz] = candidate_list[-1], candidate_list[:-1]
                            found = True
                        posx += 1
                        posz += 1
                    if ((posy > 0) and (posz > 0) and not found):
                        posy -= 1
                        posz -= 1
                        candidate_list = tensor[posx][posy][posz]
                        if (len(candidate_list) > 0):
                            if use_mse:
                                res_num[i, j], tensor[posx][posy][
                                    posz] = best_mse(candidate_list, imgcol, data)
                            else:
                                res_num[i, j], tensor[posx][posy][posz] = candidate_list[-1], candidate_list[:-1]
                            found = True
                        posy += 1
                        posz += 1
                    # Pas de patch dans les 12 diagonales
                    if not found:
                        missing += 1
                        res_num[i, j] = 1

    res = np.zeros((1024, 1024, 3), dtype=int)  # Create a matrix 1024*1024*3 to store the new RGB image

    for i in range(NsI):
        for j in range(NsI):
            patch_fill = res_num[i, j]  # Retrieving the position of the patch
            patch = data[patch_fill]  # Getting the patch
            res[i * NsI:(i + 1) * NsI, j * NsI:(j + 1) * NsI] = patch  # Filling the right portion of image with our chosen patch

    save_image(res, output_path)

