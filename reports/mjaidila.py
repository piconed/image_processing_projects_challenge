import numpy as np
import math
from tqdm import tqdm

from src.loading import load_image, save_image


def original_patches_mean_function(img):
    image_array = np.array(img)

    original_patches = []
    for l in range(0, image_array.shape[0], 32):
        for c in range(0, image_array.shape[1], 32):
            o_patch = image_array[l:l + 32, c:c + 32]
            original_patches.append(o_patch)

    original_patches = np.array(original_patches)

    original_patches_mean = np.mean(original_patches, (1, 2))  # moyenne sur les patches ( chaque moyenne est un vecteur RGB)
    return original_patches, original_patches_mean


def patches_mean_function(patches_path, n_patches):
    patches_load = np.load(patches_path)
    if n_patches is not None:
        patches_load = patches_load[:n_patches]
    # Prends les moyennes des patches (Pour chaque patch la moyenne est un vecteur RGB)
    patches_load_mean = np.mean(patches_load, (1, 2))
    return patches_load, patches_load_mean


def classement_patches(patches_path, n_patches):
    hash_map = []
    patches_load_mean = patches_mean_function(patches_path, n_patches)[1]
    for j in range(0, 45):
        hash_map.append([])  # index of patch for which color i is between 10*j and 11*j

    for i in tqdm(range(0, patches_load_mean.shape[0])):
        value_red = patches_load_mean[i][0]
        value_green = patches_load_mean[i][1]
        value_blue = patches_load_mean[i][2]
        module = math.floor(math.sqrt(
            (value_red ** 2 + value_green ** 2 + value_blue ** 2) / 100))
        hash_map[module].append(i)

    # hash_map = np.array(hash_map)
    return hash_map


def psnr(img_reference, img_compared, d=255):
    mse = np.mean((img_reference - img_compared) ** 2,
                  (0, 1))  # ligne de complexité O(1024)
    mse = 1 / 3 * sum(mse)
    return 10 * np.log10(d ** 2 / mse)


"""psnr_mean est de complexité constante, elle calcule l'erreure quadratique entre deux vecteurs RGB
(c'est juste une différence au carrée)
"""


def psnr_mean(mean_img_reference, mean_img_compared, d=255):
    mse = (mean_img_reference - mean_img_compared) ** 2  # Ligne de complexité constante
    mse = 1 / 3 * sum(mse)
    return 10 * np.log10(d ** 2 / mse)


def mse_mean(mean_img_reference, mean_img_compared, d=255):
    mse = (mean_img_reference - mean_img_compared) ** 2
    mse = 1 / 3 * sum(mse)
    return mse


""" Fonction nécessaires au code optimisé"""


def trouver_liste_indices_rec(hashmap, module, parent_list):
    cpt = 0
    for i in range(0, len(hashmap[module])):
        if hashmap[module][i] in parent_list:  # complexité O(1) car c'est un set ( le parent_list )
            cpt += 1
    if (cpt == len(hashmap[module])):
        return [-1], 0
    else:
        return hashmap[module], 1


def trouver_liste_indices(hashmap, module, parent_list):
    a = module
    b = 44 - module
    sol = trouver_liste_indices_rec(hashmap, module, parent_list)
    if sol[1] == 1:
        return sol[0]
    inc = 1
    liste_indices = []
    continuer = 1
    while continuer == 1 and inc <= 45:
        if inc <= b:
            sol = trouver_liste_indices_rec(hashmap, module + inc, parent_list)
            if sol[1] == 1:
                liste_indices += sol[0]
                continuer = 0
        if inc <= a:
            sol = trouver_liste_indices_rec(hashmap, module - inc, parent_list)
            if sol[1] == 1:
                liste_indices += sol[0]
                continuer = 0
        inc += 1
        if (inc > 45):
            print("error")
    return liste_indices


def run_reconstruction(
        image_path: str,
        patches_path: str,
        output_path: str,
        n_patches: int = None,
) -> None:

    image = load_image(image_path)

    # Performing the reconstruction.
    #On trouve les patches originaux
    original_patches=original_patches_mean_function(image)
    original_patches_load,original_patches_mean=original_patches[0],original_patches[1]

    #On classe les 100 000 patches :
    hash_map=classement_patches(patches_path, n_patches)

    #On trouve les means des patches :
    patches=patches_mean_function(patches_path, n_patches)
    patches_load,patches_load_mean=patches[0],patches[1]

    """ Hash map algorithme pour la version PSNR """
    used_indices = set()
    best_psnr=0
    index=np.zeros(1024, dtype=int)
    for i in range(0,original_patches_load.shape[0]):
        value_red=original_patches_mean[i][0]   #permet d'avoir l'indice ou rechercher dans la hashmap
        value_green=original_patches_mean[i][1]  #pour ne plus perdre de temps à chercher pour rien
        value_blue=original_patches_mean[i][2]
        module=math.floor(math.sqrt((value_red**2+value_green**2+value_blue**2)/100))
        liste_indices=trouver_liste_indices(hash_map,module,used_indices)
        for j in range(0,len(liste_indices)):
            psnr_calculated=psnr_mean(original_patches_mean[i],patches_load_mean[liste_indices[j]])
            if(not liste_indices[j] in used_indices and index[i] in used_indices):
                best_psnr=psnr_calculated
                index[i]=liste_indices[j]
            if (psnr_calculated>=best_psnr and not liste_indices[j] in used_indices):
                best_psnr=psnr_calculated
                index[i]=liste_indices[j]
        used_indices.add(index[i])
        best_psnr=0

    res = np.zeros((1024, 1024, 3),dtype=np.uint8)

    # Définir le nombre de patches par ligne et par colonne
    num_patches_per_row = 1024 // 32
    num_patches_per_col = 1024 // 32

    # Parcourir tous les patches et les insérer dans l'image reconstruite
    k=0
    for i in range(num_patches_per_row):
        for j in range(num_patches_per_col):
            x = i*32  # Déterminer la position x dans l'image reconstruite
            y = j*32  # Déterminer la position y dans l'image reconstruite
            patch = patches_load[index[k]]
            res[x:x+32, y:y+32] = patch  # Insérer le patch dans l'image reconstruite
            k+=1

    save_image(res, output_path)
