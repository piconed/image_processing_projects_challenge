import numpy as np
import skimage
from PIL import Image
import os
import pathlib
from tqdm import tqdm

from src.validation import psnr
from src.loading import load_image, save_image


def scalar_product(query, patch):
    return (np.sqrt((np.mean(query[:,:,0])-np.mean(patch[:,:,0]))**2+(np.mean(query[:,:,1])-np.mean(patch[:,:,1]))**2+(np.mean(query[:,:,2])-np.mean(patch[:,:,2]))**2))


def run_reconstruction(
        image_path: str,
        patches_path: str,
        output_path: str,
        n_patches: int = None,
) -> None:
    # Performing the reconstruction.

    ### Added code
    project_path = pathlib.Path(__file__).resolve().parents[1]
    temp_save = project_path / "data" / "temp" / "doussain"
    temp_save.mkdir(exist_ok=True)
    temp_save = f"{temp_save}"
    image = load_image(image_path)

    ### initialization
    query_size = 32 #set query size
    index_list_of_selected_patches=[]# List to store the selected patch indices
    max_psnr=[0]*1024 # List to store the maximum PSNR values (between a given query and the patches) for each patch position
    min_scalar_product=[500]*1024 # List to store the minimum scalar product (between a given query and the patches) values for each patch position
    c=0 # Counter variable
    patches = np.load(patches_path) # Load the patches
    if n_patches is not None:
        patches = patches[:n_patches]
    
    ###Extraction of queries 
    # Loop through the image and extract queries
    for i in range(0, image.shape[0], query_size):
        for j in range(0, image.shape[1], query_size):
            box = (i, j, i+query_size, j+query_size)
            query = Image.fromarray(image[box[0]:box[2], box[1]:box[3]]) # Extract the query
            filename = f"query_bis_{i}_{j}.png"
            query.save(os.path.join(temp_save, filename))  # Save the query in the folder query_bis
    
    ###selection of patches

    # Loop through the patches and select the best matches
    for i in tqdm(range(0, 1024, query_size)):
        line_number=i//32
        
        # Determine the range of patches to search based on the line number
        if line_number<8 :
            research_start=0
            research_end=n_patches // 4
        if line_number>=8 and line_number<16 :
            research_start=n_patches // 4
            research_end= n_patches // 2
        if line_number>=16 and line_number<24 :
            research_start= n_patches // 2
            research_end= (3*n_patches) // 4
        if line_number>=24 and line_number<32 :
            research_start=(3*n_patches) // 4
            research_end=n_patches
            
        # Loop through the queries and compare them with patches
        for j in range(0, 1024, query_size):
            folder_path = f'{temp_save}/query_bis_{i}_{j}.png'
            current_query = skimage.io.imread(folder_path)
            
            # Loop through the patches within the selected range
            for k in range(research_start, research_end):
                current_patch= patches[k]
                current_psnr = psnr(current_query,current_patch,d=255)
                current_scalar_product = scalar_product(current_query, current_patch)
                
                # Check conditions for selecting the patch
                cond1=(current_psnr > max_psnr[c]) # Check if current PSNR is greater than the maximum PSNR at the position
                cond2=(current_scalar_product < min_scalar_product[c])# Check if current scalar product is less than the scalar product at the position
                cond0=(k not in index_list_of_selected_patches) # Check if the patch index has not been selected before
                
                if cond0 and cond1 and cond2:
                    max_psnr[c] = current_psnr # Update the maximum PSNR at the position
                    min_scalar_product[c] = current_scalar_product # Update the minimum scalar product at the position
                    index_of_selected_patch = k # Store the index of the selected patch
                    
            index_list_of_selected_patches.append(index_of_selected_patch)# Add the selected patch index to the list
            c+=1
            
    # Create a list of selected patches based on the selected indices
    list_of_selected_patches=[patches[(index_list_of_selected_patches[p])] for p in range(1024)]  
    
    #make the mosaic
    image_concat = None
    for row in range(32):
        row_concat = None
        for col in range(32):
            index = row * 32 + col # Image index in the list
            if row_concat is None:
                row_concat = list_of_selected_patches[index]
            else:
                row_concat = np.concatenate((row_concat, list_of_selected_patches[index]), axis=1)
        if image_concat is None:
            image_concat = row_concat

        else:
            image_concat = np.concatenate((image_concat, row_concat), axis=0)

    save_image(image=image_concat, file_path=output_path)


