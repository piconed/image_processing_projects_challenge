import numpy as np
from PIL import Image
from sklearn.metrics import mean_squared_error
from tqdm import tqdm


def decouper_image_en_patch(image, taille_patch):
    image_array = np.array(image)
    hauteur, largeur, _ = image_array.shape

    patches = []
    for i in range(0, hauteur, taille_patch):
        for j in range(0, largeur, taille_patch):
            patch = image_array[i:i+taille_patch, j:j+taille_patch, :]
            patches.append(patch)

    return patches


def comparer_similarite_patch(patch_image, patches_web, used_patches, threshold=20):
    patch_image_couleurr = np.mean(patch_image, axis=(0, 1))
    similarities = np.zeros(len(patches_web))

    for i, patch in enumerate(patches_web):
        if i in used_patches:
            similarities[i] = np.inf
            continue
        patch_web_couleur = np.mean(patch, axis=(0, 1))
        similarite_couleur = np.linalg.norm(patch_image_couleurr - patch_web_couleur)

        if similarite_couleur <= threshold:
            patch_image_redimensionne = np.array(patch_image).reshape(-1)
            patch_web_redimensionne = np.array(patch).reshape(-1)
            similarities[i] = mean_squared_error(patch_image_redimensionne, patch_web_redimensionne)
        else:
            similarities[i] = np.inf

    best_patch_index = np.argmin(similarities)
    used_patches.add(best_patch_index)
    return best_patch_index


def trier_patch_web_par_couleur(patches_web):
    nombre_patch = len(patches_web)
    couleur = np.zeros((nombre_patch, 3))
    patch_triee = []

    for i in range(nombre_patch):
        patch = patches_web[i]
        couleur[i] = np.mean(patch, axis=(0, 1))

    indice_triee = np.argsort(couleur[:, 0])

    for i in indice_triee:
        patch_triee.append(patches_web[i])

    return patch_triee



def image_reconstruite(img, patch_image, patches_web, taille_patch):
    num_patches_h, num_patches_l = img.size[0] // taille_patch, img.size[1] // taille_patch
    image_reconstruite = np.zeros_like(img)
    used_patches = set()

    for i in tqdm(range(num_patches_h)):
        for j in range(num_patches_l):
            patch_considere = patch_image[i * num_patches_l + j]
            index_meilleur_patch = comparer_similarite_patch(patch_considere, patches_web,used_patches)
            meilleur_patch = patches_web[index_meilleur_patch]

            row = i * taille_patch
            col = j * taille_patch
            image_reconstruite[row:row+taille_patch, col:col+taille_patch, :] = meilleur_patch

    return image_reconstruite


def run_reconstruction(
        image_path: str,
        patches_path: str,
        output_path: str,
        n_patches: int = None,
) -> None:

    img = Image.open(image_path)
    patches_web = np.load(patches_path)
    if n_patches is not None:
        patches_web = patches_web[:n_patches]

    #Essai
    patches_web = trier_patch_web_par_couleur(patches_web)
    patch_size = 32
    patches = decouper_image_en_patch(img, patch_size)
    reconstructed_img = image_reconstruite(img, patches, patches_web, patch_size)
    Image.fromarray(reconstructed_img).save(output_path)


