import numpy as np
from tqdm import tqdm

from src.loading import load_patches, load_image, save_image


def PSNR(img_reference, img_compared, d=255):
    mse = np.mean((img_reference - img_compared) ** 2)
    return 10 * np.log10(d**2 / mse)


def comparison_element_psnr(subset_element, patches, patch_mask):
    psnr = 0
    index_best_patch=0
    best_patch = patches[0]
    for i in range(patches.shape[0]):
        if patch_mask[i]!=True:
            # currently we used only the PSNR (it's a test) -> on pourrait utiliser d'autres critères en plus de ressemblances
            psnr_i = PSNR(subset_element, patches[i])
            if(psnr_i >= psnr):
                psnr = psnr_i
                best_patch = patches[i]
                index_best_patch = i
    return best_patch, index_best_patch


#Methode PSNR
def association_query_patch_psnr(subset, patches, patch_mask):
    liste_patches = []
    for i in tqdm(range(len(subset))):
        for j in range(len(subset[0])):
            best_patch, index_best_patch = comparison_element_psnr(subset[i][j], patches, patch_mask)
            # Marquage du patch sélectionné comme utilisé dans le masque
            patch_mask[index_best_patch] = True
            liste_patches.append(best_patch)
    return liste_patches


def decomposition(img):
    img_subset_width = 32  # in px
    img_subset_height = 32  # in px
    nb_img_subset_cols = int(img.shape[0] / img_subset_height)  # nb image for create one colum of the original image
    nb_img_subset_rows = int(img.shape[1] / img_subset_width)
    subset = []
    for i in range(nb_img_subset_cols):
        subset_row = []
        for j in range(nb_img_subset_rows):
            img_element = img[i*32:(i+1)*32, j*32:(j+1)*32, :]  # slicing [height, width, rgb]
            subset_row.append(img_element)
        subset.append(subset_row)
    return subset


def run_reconstruction(
        image_path: str,
        patches_path: str,
        output_path: str,
        n_patches: int = None,
) -> None:

    patches = load_patches(patches_path, n_patches)
    img = load_image(image_path)

    patch_mask = np.zeros(patches.shape[0], dtype=bool)

    subset_img = decomposition(img)
    liste_patches = association_query_patch_psnr(subset_img, patches, patch_mask)

    # Image dimmension and patches
    img_height, img_width, img_channels = img.shape
    patch_height, patch_width, patch_channels = liste_patches[0].shape

    # Number of patch ine the row and the cols
    num_patches_row = img_height // patch_height
    num_patches_col = img_width // patch_width

    # empty image for the reconstruction
    res = np.zeros((img_height, img_width, img_channels), dtype=np.uint8)

    # index to follow the patch position in the list
    patch_index = 0

    # Parcours de chaque patch dans l'image reconstruite
    for row in range(num_patches_row):
        for col in range(num_patches_col):
            # Coordonnées de départ du patch dans l'image reconstruite
            start_row = row * patch_height
            start_col = col * patch_width

            # Copie du patch correspondant à partir de la liste_patches vers l'image reconstruite
            res[start_row:start_row+patch_height, start_col:start_col+patch_width, :] = liste_patches[patch_index]

            # Incrémentation de l'indice du patch
            patch_index += 1

    save_image(res, output_path)
