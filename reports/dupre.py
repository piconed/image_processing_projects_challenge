# Imports and configurations
import numpy as np
import cv2
from skimage.metrics import structural_similarity as ssim
from tqdm import tqdm

from src.loading import save_image


def extract_patches(image, patch_size):
    patches = []
    height, width, _ = image.shape
    for y in range(0, height, patch_size):
        for x in range(0, width, patch_size):
            patch = image[y:y+patch_size, x:x+patch_size, :]
            patches.append(patch)
    return patches


def calculate_similarity(image1, image2, metric):
    metric = metric.lower()

    if metric == "mse":
        mse = np.mean(np.square(image1 - image2), axis=(0, 1, 2))
        return mse

    elif metric == "psnr":
        mse = np.mean(np.square(image1 - image2), axis=(0, 1, 2))
        psnr = 20 * np.log10(255.0 / np.sqrt(mse))
        return psnr

    elif metric == "ssim":
        ssim_score = ssim(image1, image2, multichannel=True)
        return ssim_score

    elif metric == "histogram":
        hist1 = cv2.calcHist([image1], [0, 1, 2], None, [8, 8, 8], [0, 256, 0, 256, 0, 256])
        hist2 = cv2.calcHist([image2], [0, 1, 2], None, [8, 8, 8], [0, 256, 0, 256, 0, 256])
        return cv2.compareHist(hist1, hist2, cv2.HISTCMP_CHISQR)


def run_reconstruction(
        image_path: str,
        patches_path: str,
        output_path: str,
        n_patches: int = None,
) -> None:

    image = cv2.imread(image_path)  # Load the image using OpenCV
    image_rgb = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)  # Convert BGR to RGB

    # Reduce the size of the image
    reduced_image = cv2.resize(image_rgb, (image_rgb.shape[1] // 1, image_rgb.shape[0] // 1))  # Adjust the scale factor as needed

    patches = np.load(patches_path)
    if n_patches is not None:
        patches = patches[:n_patches]

    patch_size = 32  # Size of the patches
    query_patches = extract_patches(reduced_image, patch_size)

    reconstructed_image = np.zeros_like(reduced_image)  # Initialize the reconstructed image
    height, width, _ = reduced_image.shape  # Get the height and width of the reduced image

    used_patch_indices = []  # List to store the indices of used patches

    for y in tqdm(range(0, height, patch_size)):
        for x in range(0, width, patch_size):
            query_patch = reduced_image[y:y+patch_size, x:x+patch_size, :]

            best_similarity = float('inf')
            best_patch_index = None

            for i, patch in enumerate(patches):
                if i not in used_patch_indices:  # Check if the patch index has already been used
                    similarity = calculate_similarity(query_patch, patch, metric="psnr")

                    if similarity < best_similarity:
                        best_similarity = similarity
                        best_patch_index = i

            best_patch = patches[best_patch_index]

            # Place the best-fitting patch in the reconstructed image
            reconstructed_image[y:y+patch_size, x:x+patch_size, :] = best_patch

            used_patch_indices.append(best_patch_index)  # Add the index of the used patch to the list

    # Resize the reconstructed image to the original size
    reconstructed_image = cv2.resize(reconstructed_image, (image_rgb.shape[1], image_rgb.shape[0]))
    save_image(reconstructed_image, output_path)
