import numpy as np
from src.loading import load_image, save_image, load_patches
from tqdm import tqdm


def error(img1 , img2):
    return np.sum(np.abs(img1 - img2))


def run_reconstruction(
        image_path: str,
        patches_path: str,
        output_path: str,
        n_patches: int = None,
) -> None:

    patch_test = load_patches(patches_path, n_patches)

    # image Choise  :
    img = load_image(image_path)

    # Divide the image into 1024 fragments (=> list_morceaux) :
    nligne, ncol, rgb = img.shape

    cote_haut_carre = list(range(0, nligne, 32))
    cote_gauche_carre = list(range(0, ncol, 32))
    list_morceaux = [[0 for j in cote_gauche_carre] for i in cote_haut_carre]

    for i in cote_haut_carre:
        for j in cote_gauche_carre:
            list_morceaux[i // 32][j // 32] = img[i:i + 32, j:j + 32]

    error_distances = np.array([[[error(list_morceaux[i][j], patch)  for patch in patch_test] for j in range(32)] for i in tqdm(range(32))])

    selected_indices = np.argmin(error_distances, axis=-1)
    selected_patch = [[patch_test[selected_indices[i, j]] for j in
                       range(selected_indices.shape[1])] for i in
                      range(selected_indices.shape[0])]

    # Displaying the result  :
    list_j = []
    for i in range(0, 32):
        list_j.append(np.hstack(selected_patch[i]))
    reconstructed_img = np.vstack(list_j)
    save_image(reconstructed_img, output_path)
