import numpy as np
from src.loading import load_patches, load_image, save_image
from skimage import img_as_float
from skimage.metrics import structural_similarity as ssim
from tqdm import tqdm


def slice_image(image: np.ndarray, dim: int):
    """Découpe l'image en carrés de taille dim x dim

    Args:
        image (np.ndarray): L'image originale que l'on veut reconstruire
        dim (int): La taille des patches en lesquels l'image sera découpée (32 ici)

    Returns:
        sliced_tab: Tableau qui contient les patches de l'image dans l'ordre
                    élément [0] --> ligne [0] ; colonne [0]
                    élément [1] --> ligne [0] ; colonne [1]
                    etc
    """

    # On prend les dimensions de l'image
    nb_rows, nb_cols = image.shape[:2]

    # On calcule combien de patches rentrent dans l'image
    nb_carres_largeur = nb_cols // dim
    nb_carres_hauteur = nb_rows // dim

    # si les dimensions de l'image ne sont pas des multiples de dim alors nb_carres_largeur ou nb_carres_hauteur ne vaudront pas 0
    # on recalcule alors un nombre de lignes ou de colonnes pour que l'image aient des dimensions qui soient des multiples de dim
    if nb_carres_largeur > 0:
        nb_cols = nb_carres_largeur * dim
    if nb_carres_hauteur > 0:
        nb_rows = nb_carres_hauteur * dim

    # Si le nombre de lignes ou de colonnes ont été changé alors on redimensionne l'image
    if image.shape[0] != nb_rows or image.shape[1] != nb_cols:
        image = image[:nb_rows, :nb_cols]

    # tableau pour ranger les patchs
    sliced_tab = []

    # Parcourir chaque carré
    for i in range(nb_carres_largeur):
        for j in range(nb_carres_hauteur):
            x1 = dim * i
            y1 = dim * j
            x2 = x1 + dim
            y2 = y1 + dim
            # On prend le patch de l'image et on le range dans un tableau
            cropped = image[x1:x2, y1:y2]
            sliced_tab.append(cropped)

    return sliced_tab


def calc_hist_patches(patches: []):
    """calcule les histogrammes de couleur des patchs du tableau de patchs donné en entrée

    Args:
        patches ([]) : Tableau contenant les patches dont on veut calculer les histogrammes

    Returns:
        histogrammes : array de taille (len(patches), 3, 256) qui contient tous les histogrammes de chaque patch
                       Rouge, vert et bleu
    """

    # Tableau pour stocker les histogrammes des canaux RGB
    histogrammes = np.zeros((len(patches), 3, 256))

    # on parcourt les patchs
    for i in range(len(patches)):
        channels = np.dsplit(patches[i], patches[i].shape[2])

        histograms = []
        k = 0

        # on parcourt chaque channel du patch un à un pour calculer son histogramme
        for channel in channels:
            hist, bins = np.histogram(channel.flatten(), bins=256,
                                      range=[0, 256])
            histograms.append(hist)
            histograms[k] = histograms[k] / float(
                patches[i].size)  # normalisation de l'histogramme
            histogrammes[i, k, :] = histograms[
                k]  # on range l'histogramme à la bonne place dans le tableau à retourner
            k = k + 1

    return histogrammes


def compare_color(image: np.ndarray, patches: [], histogrammes_patches,
                  indices: [], size_tab: int):
    """compare une image de taille dim x dim avec un nombre de patchs spécifié
       garde les meilleurs candidats selon la valeur de ccs
       ccs proche de 3 --> image identique
       ccs proche de -3 --> image inverse
       (car on a trois canaux de couleur)

    Args:
        image (np.ndarray) : Le carré que l'on veut comparer avec la banque d'images
        patches ([]) : Tableau contenant les patches avec lesquels on veut comparer
        histogrammes_patches : les histogrammes des patchs avec lesquels on veut comparer notre image
        indices ([]) : les indices des patchs de la banque de données avec lesquels on compare notre image de taille dim x dim (patch de l'image)
        size_tab (int) : nombre de patches que l'on veut retenir suite à cette comparaison

    Returns:
        tab_patches : tableau contenant les patchs que l'on a retnu suite à cette comparaison
        tab_patches_indices  : indices des patchs retenus

    """

    # On calcule l'histogramme de notre patch d'image à comparer
    # les channels sont les trois canaux de l'image (R, G, B)
    channels = np.dsplit(image, image.shape[2])

    histogramme_reference = np.zeros((3, 256))

    # boucle for qui permet de créer un histogramme pour chaque canal de l'image
    for i, channel in enumerate(channels):
        hist, bins = np.histogram(channel.flatten(), bins=256, range=[0, 256])
        histogramme_reference[i] = hist
        # normalisation de l'histogramme
        histogramme_reference[i] = histogramme_reference[i] / float(image.size)

    # tableaux pour stocker ce que l'on veut retourner
    tab_patches = []
    tab_patches_indices = []

    tab_cor_coef = []

    # On calcule le coefficient de corrélation de similarité pour chaque canal de couleur
    # entre notre patch et tous les patches de patches : [] puis on somme pour chaque canal
    # On a donc un résultat entre -3 et 3

    for k in range(len(patches)):
        cor_coef_simi = 0
        for l in range(image.shape[2]):
            cor_coef_simi = cor_coef_simi + correlation_similarity(
                histogramme_reference[l], histogrammes_patches[k, l])

        if len(tab_cor_coef) == size_tab:  # si le tableau est plein
            if cor_coef_simi > np.min(tab_cor_coef):
                # on regarde si le ssim de ce patch est supérieur au min de notre tableau
                # Si oui on remplace l'élément avec le plus petit cor_coef_simi du tableau par le nouveau patch
                # (qui est donc plus correspondant)
                min_idx = np.argmin(tab_cor_coef)
                tab_cor_coef[
                    min_idx] = cor_coef_simi  # np.argmin nous donne la position du minimum dans le tableau
                tab_patches[min_idx] = patches[k]
                tab_patches_indices[min_idx] = indices[k]

        else:
            # si le tableau n'est pas plein on rajoute le patch dans le tableau tab_patches
            # et le cor_coef_simi de ce patch dans le tableau tab_cor_coef
            # pour pouvoir comparer quand on revient dans la boucle si le tableau est plein
            tab_cor_coef.append(cor_coef_simi)
            tab_patches.append(patches[k])
            tab_patches_indices.append(indices[k])

    return tab_patches, tab_patches_indices


def correlation_similarity(hist1, hist2):
    """calcule le coefficient de similarité entre deux histogrammes
       similarity est compris entre -1 et 1

    Args:
        hist1 (np.ndarray(256)) : premier histogramme à comparer
        hist2 (np.ndarray(256)) : deuxième histogramme à comparer

    Returns:
        similarity : le coefficient de similarité entre les deux histogrammes
    """

    # Vérification des dimensions des histogrammes
    assert hist1.shape == hist2.shape, "Les histogrammes doivent avoir la même dimension."

    # Extraction du coefficient de similarité
    similarity = np.corrcoef(hist1, hist2)[0, 1]

    return similarity


def rank_to_pos(rang: int, dim: int):
    """Donne la position en deux dimensions du patch qui est rangé à la position rang
       dans un tableau de taille dim x dim (nombre de patchs utilisés pour recréer l'image)

    Args:
        rang (int) : rang du patch dans le tableau
        dim (int) : taille des patchs

    Returns:
        position ((int, int)) : la position du patch sur l'image
    """

    position = (rang // dim, rang % dim)
    # % opérateur modulo --> donne le reste de la division euclidienne de rang par dim
    # est équivalent à :
    # position[0] = rang // dim
    # position[1] = rang - dim*position[0]

    return position


def compare_ssim(image: np.ndarray, patches: [], indices: [], size_tab: int):
    """Compare une image de taille dim x dim avec un nombre de patchs spécifié
       et garde les meilleurs candidats selon la valeur de SSIM (Structural Similarity Index)

    Args:
        image (np.ndarray): L'image que l'on veut comparer avec les patches
        patches ([]): Tableau contenant les patches avec lesquels on veut comparer
        indices ([]): Les indices des patches de la banque de données avec lesquels on compare notre image
        size_tab (int): Nombre de patches que l'on veut retenir suite à cette comparaison

    Returns:
        tab_patches ([]): Tableau contenant les patches que l'on a retenus suite à cette comparaison
        tab_patches_indices ([]): Indices des patches retenus
    """

    img_cropped = img_as_float(image)  # on convertit l'image de taille dim x dim en float
    # déclaration des tableaux pour stocker les différents patches retenus et leur ssim
    # taille que l'on peut spécifier --> correspond au nombre de patches que l'on veut garder après ce test
    tab_ssim = []
    tab_patches = []
    tab_patches_indices = []

    # on parcourt la liste de patches disponibles
    for i in range(len(patches)):
        ssim_const = 0
        patch = img_as_float(patches[i])  # on convertit le patch en float
        # calcul du coefficient de similarité de structure
        ssim_const = ssim(img_cropped, patch, win_size=3, data_range=1, multichannel=True)

        if len(tab_ssim) == size_tab:  # si le tableau est plein
            if ssim_const > np.min(tab_ssim):
                # on regarde si le ssim de ce patch est supérieur au min de notre tableau
                # Si oui on remplace l'élément avec le plus petit ssim du tableau par le nouveau patch (qui est donc plus correspondant)
                min_idx = np.argmin(tab_ssim)
                tab_ssim[
                    min_idx] = ssim_const  # np.argmin nous donne la position du minimum dans le tableau
                tab_patches[min_idx] = patches[i]
                tab_patches_indices[min_idx] = indices[i]

        else:
            # si le tableau n'est pas plein on rajoute le patch qui rempli le critère ssim_const > ssim dans le tableau tab_patches
            # et le ssim de ce patch dans le tableau tab_ssim
            # pour pouvoir comparer quand on revient dans la boucle si le tableau est plein
            tab_ssim.append(ssim_const)
            tab_patches.append(patches[i])
            tab_patches_indices.append(indices[i])

    return tab_patches, tab_patches_indices


def compare_psnr(img_reference: np.ndarray, patches: [], indices: [],
                 size_tab: int, d=255):
    """Compare une image de référence avec un nombre de patches spécifié
       et garde les meilleurs candidats selon la valeur de PSNR (Peak Signal-to-Noise Ratio)

    Args:
        img_reference (np.ndarray): L'image de référence
        patches ([]): Tableau contenant les patches avec lesquels on veut comparer
        indices ([]): Les indices des patches de la banque de données avec lesquels on compare notre image
        size_tab (int): Nombre de patches que l'on veut retenir suite à cette comparaison
        d (int): Gamme de valeurs (par défaut, 255 pour des images en 8 bits)

    Returns:
        tab_patches ([]): Tableau contenant les patches que l'on a retenus suite à cette comparaison
        indices_patches ([]): Indices des patches retenus
    """

    tab_patches = []
    tab_psnr = []
    indices_patches = []

    for i in range(len(patches)):
        patch = patches[i]

        # on calcule le psnr
        mse = np.mean((img_reference - patch) ** 2)
        psnr = 10 * np.log10(d ** 2 / (mse + 1e-10))

        if len(tab_psnr) == size_tab:  # si le tableau est plein
            if psnr > np.min(tab_psnr):
                # on regarde si le psnr de ce patch est supérieur au min de notre tableau
                # Si oui on remplace l'élément avec le plus petit psnr du tableau par le nouveau patch (qui est donc plus correspondant)
                min_idx = np.argmin(tab_psnr)
                tab_psnr[
                    min_idx] = psnr  # np.argmin nous donne la position du minimum dans le tableau
                tab_patches[min_idx] = patches[i]
                indices_patches[min_idx] = indices[i]

        else:
            # si le tableau n'est pas plein on append juste dans les différents tableaux
            # pour pouvoir comparer quand on revient dans la boucle si le tableau est plein
            tab_psnr.append(psnr)
            tab_patches.append(patches[i])
            indices_patches.append(indices[i])

    return tab_patches, indices_patches


def run_reconstruction(
        image_path: str,
        patches_path: str,
        output_path: str,
        n_patches: int = None,
) -> None:
    dim = 32
    image = load_image(image_path)

    # Récupération des dimensions de l'image
    nb_rows = image.shape[0]
    nb_cols = image.shape[1]

    # Initialisation de l'image reconstruite
    image_reconstructed = np.zeros((nb_rows, nb_cols, 3))

    # Découpage de l'image de référence en patches
    sliced_tab = slice_image(image, dim)

    # Chargement des patches à partir de la banque d'images
    patches = load_patches(patches_path, n_patches)
    length_patches = len(patches)

    # Sélection de length_patches indices aléatoires
    indices_aleatoires = np.random.choice(len(patches), size=length_patches, replace=False)
    # Récupération des patches correspondants aux indices sélectionnés
    # On a donc length_patches patches différents choisis parmi 1 million qui changent pour chaque reconstruction
    patches = patches[indices_aleatoires]
    # Création des indices correspondants
    indices = np.arange(length_patches)

    for i in tqdm(range(len(sliced_tab))):

        # Comparaison des patches avec PSNR
        # on garde 250 patches suite à cette comparaison
        tab_patches_psnr, indices_patches_psnr = compare_psnr(sliced_tab[i], patches, indices, 250, d=255)

        # Comparaison des patches avec SSIM
        # on garde 50 patches suite à cette comparaison
        tab_compare_ssim, tab_patches_indices_ssim = compare_ssim(
            sliced_tab[i], tab_patches_psnr, indices_patches_psnr, 50)

        # Calcul des histogrammes des patches
        histogrammes_patches = calc_hist_patches(tab_compare_ssim)

        # Comparaison des patches avec la couleur
        # on garde un seul patch donc elui qui est le meilleur candidat
        tab_patch_compare_color, tab_patches_indices = compare_color(
            sliced_tab[i], tab_compare_ssim, histogrammes_patches,
            tab_patches_indices_ssim, 1)

        # Calcul de la position du patch dans l'image reconstruite
        position = rank_to_pos(i, dim)

        # Ajout du patch à l'image reconstruite
        image_reconstructed[dim * position[0]:dim * position[0] + dim,
        dim * position[1]:dim * position[1] + dim, :] = tab_patch_compare_color[0]

        patches = np.delete(patches, tab_patches_indices[0], axis=0)

    # Normalisation de l'image reconstruite
    # image_reconstructed = image_reconstructed.astype(np.float32) / 255.0

    save_image(image_reconstructed, output_path)
