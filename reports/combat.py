import numpy as np
from skimage.util import img_as_ubyte
from tqdm import tqdm

from src.loading import load_image, save_image, load_patches


def Image_mean_color_block(img, sizeblock):
    pixelised_img = np.copy(img)
    lx = img.shape[0]
    ly = img.shape[1]
    nb_block_x = lx//sizeblock
    nb_block_y = ly//sizeblock
    for i in range(nb_block_x):
        for j in range(nb_block_y):
            for l in range(3):
                mean = np.mean(img[(i*sizeblock):(i*sizeblock+sizeblock),
                               (j*sizeblock):(j*sizeblock+sizeblock), l])
                pixelised_img[(i*sizeblock):(i*sizeblock+sizeblock),
                              (j*sizeblock):(j*sizeblock+sizeblock), l] = mean
    return(pixelised_img, nb_block_x, nb_block_y)


def open_input(N, patches):
    liste_mean_input = []
    for k in range(N):
            patch=patches[k]
            mean = [0., 0., 0.]
            for rgb in range(3):
                mean[rgb] = np.mean(patch[:, :, rgb])
            liste_mean_input.append(np.array(mean))
    return liste_mean_input


def dist_eucl3(v):
    return(np.sqrt(v[0]*v[0] + v[1]*v[1] + v[2]*v[2]))


def dist_eucl3_list(lv):
    n = len(lv)
    res=np.zeros(n)
    for k in range (n):
        res[k] = dist_eucl3(lv[k])
    return res


def run_reconstruction(
        image_path: str,
        patches_path: str,
        output_path: str,
        n_patches: int = None,
) -> None:

    patches = load_patches(patches_path, n_patches)
    N = len(patches)

    size = patches[0].shape  # on admet qu'ils sont carrés
    size = size[0]
    image = load_image(image_path)
    mosaique = np.copy(image)

    img_pixelised, nb_block_x, nb_block_y = Image_mean_color_block(image, size)

    list_mean_input = open_input(N, patches)

    index = list(np.linspace(0, N, N + 1).astype(int))

    for i in tqdm(range(nb_block_x)):
        for j in range(nb_block_y):
            mean_of_block = img_pixelised[i * size, j * size]

            # Recherche de la moyenne minimale (son indice)
            kmin = np.argmin(dist_eucl3_list(list_mean_input - mean_of_block))

            # Resize the selected image and
            image_resized = img_as_ubyte(patches[index[kmin]])
            for i_r in range(size):
                for j_r in range(size):
                    for rgb in range(3):
                        mosaique[i * size + i_r, j * size + j_r, rgb] = image_resized[i_r, j_r, rgb]
            index.pop(kmin)
            list_mean_input.pop(kmin)

    save_image(mosaique, output_path)
