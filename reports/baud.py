import pathlib

import numpy as np
from tqdm import tqdm

from src.loading import load_image, save_image
from src.validation import psnr


def run_reconstruction(
    image_path: str,
    patches_path: str,
    output_path: str,
    n_patches: int = None,
) -> None:

    project_path = pathlib.Path(__file__).resolve().parents[1]
    temp_save = project_path / "data" / "temp" / "baud"
    temp_save.mkdir(exist_ok=True)
    temp_save = f"{temp_save / 'original_patches.npy'}"
    image = load_image(image_path)

    # Conversion of the image in an array:
    image_array = np.array(image)

    # Cutting of the image according to squares of size 32x32:
    original_patches = []

    for l in range(0, image_array.shape[0], 32):
        for c in range(0, image_array.shape[1], 32):
            o_patch = image_array[l:l + 32, c:c + 32]
            original_patches.append(o_patch)

    # Backup of the patches obtained:
    np.save(temp_save, original_patches)

    # Conversion of the original patches base:
    original_patches_load = np.load(temp_save)
    original_patches_load_float = original_patches_load.astype(float)

    # Conversion of the patches database:
    patches_load = np.load(patches_path)
    if n_patches is not None:
        patches_load = patches_load[:n_patches]
    patches_load_float = patches_load.astype(float)

    # Calculate RGB distance between original patches and each of the patches from the database:
    distances = np.zeros((original_patches_load_float.shape[0], patches_load_float.shape[0]))
    for i in tqdm(range(original_patches_load_float.shape[0])):
        for j in range(patches_load_float.shape[0]):
            # Calculation is based on Euclidean distance between pixel values:
            # (we work on images converted to floats to avoid overflows when calculating RGB distance)
            distances[i, j] = np.linalg.norm(original_patches_load_float[i] - patches_load_float[j])

    # Sorting indices in order to get the best matching patch (minimum RGB distance) in first position:
    sorted_indices = np.argsort(distances, axis=1)

    # Second confirmation of the choice of the best replacement patch, based on the psnr + unique choice of patches:
    best_psnr = 0
    used_indices = []
    index = np.zeros(1024, dtype=int)
    for i in range(original_patches_load_float.shape[0]):
        for j in sorted_indices[i]:
            # Unique choice of patches:
            if j not in used_indices:
                psnr_calculated = psnr(original_patches_load_float[i], patches_load_float[j])
                # Best PSNR chosen:
                if psnr_calculated >= best_psnr:
                    best_psnr = psnr_calculated
                    index[i] = j
                used_indices.append(j)
                break
        best_psnr = 0

    # Initialization of the reconstructed image (type= uint8):
    res = np.zeros((1024, 1024, 3), dtype=np.uint8)

    # Insert the patches with the best index previously defined in the reconstructed image:
    # (we use here the type uint8 for patches)

    k = 0
    for i in range(original_patches_load.shape[1]):
        for j in range(original_patches_load.shape[2]):
            x = i * 32  # Determines the position x in the reconstructed image
            y = j * 32  # Determines the position y in the reconstructed image
            patch = patches_load[index[k]]
            res[x:x + 32, y:y + 32] = patch  # Inserting the patch into the reconstructed image
            k += 1

    save_image(res, output_path)

# 2023
# Edited by : Eloïse Baud-Julia Chêne-Mathilde Chareyron