import numpy as np
from scipy.spatial import cKDTree
from tqdm import tqdm

from src.loading import load_image, save_image
def find_best_patch(img_patch, patches):
    distances = np.linalg.norm(img_patch - patches, axis=(1, 2))
    best_index = np.argmin(distances)
    return patches[best_index]
def run_reconstruction(
        image_path: str,
        patches_path: str,
        output_path: str,
        n_patches: int = None,
) -> None:
    image = load_image(image_path)
    patches = np.load(patches_path)
    if n_patches is not None:
        patches = patches[:n_patches]
    patch_size = patches.shape[1]
    image_height, image_width, _ = image.shape
    num_patches_height = image_height // patch_size
    num_patches_width = image_width // patch_size
    reconstructed_image = np.zeros_like(image)
    patches_flat = patches.reshape(patches.shape[0], -1)
    patches_tree = cKDTree(patches_flat)
    for i in tqdm(range(num_patches_height)):
        for j in range(num_patches_width):
            start_h = i * patch_size
            start_w = j * patch_size
            end_h = start_h + patch_size
            end_w = start_w + patch_size
            img_patch = image[start_h:end_h, start_w:end_w]
            img_patch_flat = img_patch.flatten()
            _, best_index = patches_tree.query(img_patch_flat)
            best_patch = patches[best_index]
            reconstructed_image[start_h:end_h, start_w:end_w] = best_patch
    save_image(reconstructed_image, output_path)