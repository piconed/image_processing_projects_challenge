# Image processing course ASI 2A - Project results

## Introduction

The 2022/2023 projects for 2A ASI students at the ENSE3 course of Grenoble INP aimed at solving a problem of **image tiling**. 
The original project subject is available at the link below:
[Project link](https://gricad-gitlab.univ-grenoble-alpes.fr/mullemat/image_processing_project)

The project is set as a challenge among different groups of students, that had to send their solution indipendently.
By the deadline of 06/06/2023, we received 12 scripts. 

Each script is assigned an unique label `group` to identify them:

| group     | members                               |
|:----------|:--------------------------------------|
| agoumbi   | Agoumbi, Atanda, Bah, Diabate         |
| augereau  | Augereau, Charmot                     |
| baud      | Baud, Chêne, Chareyron                |
| benedetti | Benedetti, Runel, Soufargi            |
| combat    | Combat, Jouannin, Lacroix             |
| doussain  | Doussain, El Gaf, Haudrechy, Masselis |
| dupre     | Dupré, Laurencin, Machado             |
| gailly    | Gailly, Guefif, Hanol                 |
| mjaidila  | Mjaidila, Manzini, Shahin, Torres     |
| novarina  | Novarina, Rocher, Serasse, Tognellini |
| owona     | Owona, Ramone, Verstraeten            |
| pelops    | Pelops, Turki                         |

## Code style

In the original subject, students were asked various guidelines for the project.
In this document, we do not rate the actual report or the text comments of the provided code.
The table below gives a `v` if a certain requirement is satisfied or a `x` if it is not.

We rank instead the following requirements:
- `signature`: The provided code must have function with the signature requested by the subject, with the same inputs and output.
- `global`: If the code is provided by notebook, it should not use global variables, making the functions self-contained.
- `segmented`: All the main code is not in multiple cells or with interleaved functions.
- `debug`: The code did not require any changes in the line to run properly.
- `extension`: It is easy to include extra features (e.g. selecting the number of patches).
- `unique-patch`: Checks if the requirement of having non-repeating patches is satisfied.


| group     | signature   | global   | segmented   | debug | extension   | unique-patch   |
|:----------|:------------|:---------|:------------|:------|:------------|:---------------|
| agoumbi   | x           | v        | x           | v     | v           | x              |
| augereau  | x           | v        | x           | v     | v           | v              |
| baud      | x           | v        | x           | x     | v           | v              |
| benedetti | x           | x        | x           | v     | v           | v              |
| combat    | x           | v        | v           | v     | x           | v              |
| dupre     | x           | v        | x           | v     | x           | v              |
| doussain  | v           | v        | v           | v     | x           | v              |
| gailly    | x           | x        | v           | v     | v           | v              |
| mjaidila  | v           | v        | v           | x     | x           | v              |
| novarina  | x           | v        | v           | v     | v           | v              |
| owona     | x           | v        | x           | v     | v           | x              |
| pelops    | v           | v        | v           | v     | v           | x              |

## Results

The code was run over 8 images, 4 of which were originally provided to the students and 4 are completely new.
The tests were run with a reduced database of 50,000 patches, to avoid too long runtimes.
Some minor modifications were done in the provided code to allow it, but always keeping the same approach of the students.
The output images are given in the `data/output` section.

For the objective evaluation, we employ the following measures:
- `sam`: Spectral angle mapper (best results are the smallest)
- `psnr`: Peak signal to noise ratio (best results are the largest)
- `ssim`: Spectral similarity (best results are the largest)
- `time`: Computation time (best results are the smallest)

The results are below, best results in **bold**, second best is <ins>underlined</ins>.


| group     | sam                             | psnr                            | ssim                             | time                            |
|:----------|:--------------------------------|:--------------------------------|:---------------------------------|:--------------------------------|
| agoumbi   | (7.19 ± 2.72) x 10^0            | (1.53 ± 0.11) x 10^1            | (3.11 ± 0.52) x 10^-1            | (4.15 ± 0.14) x 10^2            |
| augereau  | (8.95 ± 2.03) x 10^0            | (1.31 ± 0.03) x 10^1            | (1.08 ± 0.19) x 10^-1            | <ins>(4.50 ± 0.22) x 10^1</ins> |
| baud      | (7.59 ± 2.07) x 10^0            | <ins>(1.87 ± 0.11) x 10^1</ins> | (2.62 ± 0.31) x 10^-1            | (3.04 ± 0.25) x 10^2            |
| benedetti | (1.23 ± 0.30) x 10^1            | (9.39 ± 0.45) x 10^0            | (1.71 ± 0.21) x 10^-1            | (5.97 ± 0.24) x 10^2            |
| combat    | (8.04 ± 1.78) x 10^0            | (1.33 ± 0.03) x 10^1            | (1.11 ± 0.19) x 10^-1            | (8.63 ± 0.24) x 10^1            |
| doussain  | (7.62 ± 2.03) x 10^0            | (1.52 ± 0.09) x 10^1            | (1.93 ± 0.24) x 10^-1            | (6.23 ± 0.28) x 10^2            |
| dupre     | (1.31 ± 0.31) x 10^1            | (8.34 ± 1.39) x 10^0            | (1.54 ± 0.21) x 10^-1            | (7.39 ± 0.31) x 10^2            |
| gailly    | (8.64 ± 2.80) x 10^0            | (1.47 ± 0.26) x 10^1            | (2.07 ± 0.51) x 10^-1            | (1.75 ± 0.24) x 10^3            |
| mjaidila  | (8.20 ± 1.80) x 10^0            | (1.33 ± 0.03) x 10^1            | (1.10 ± 0.17) x 10^-1            | **(1.04 ± 0.24) x 10^1**        |
| novarina  | <ins>(6.89 ± 1.83) x 10^0</ins> | (1.41 ± 0.09) x 10^1            | (1.91 ± 0.29) x 10^-1            | (1.47 ± 0.20) x 10^3            |
| owona     | (1.55 ± 0.50) x 10^1            | (1.63 ± 0.12) x 10^1            | <ins>(3.51 ± 0.49) x 10^-1</ins> | (3.27 ± 0.10) x 10^3            |
| pelops    | **(5.56 ± 1.67) x 10^0**        | **(2.12 ± 0.13) x 10^1**        | **(3.67 ± 0.58) x 10^-1**        | (1.39 ± 0.05) x 10^2            |

## Supervisors

- Matthieu Chancel: matthieu.chancel@gipsa-lab.grenoble-inp.fr
- Mauro Dalla Mura: mauro.dalla-mura@gipsa-lab.grenoble-inp.fr
- Matthieu Muller: matthieu.muller@gipsa-lab.grenoble-inp.fr
- Daniele Picone: daniele.picone@grenoble-inp.fr
