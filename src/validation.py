import dataclasses

import numpy as np
from skimage.metrics import structural_similarity
import pandas as pd

from src.loading import load_image


def psnr(img_reference, img_compared, d=255):
    img_compared = img_compared.astype(np.uint8)
    mse = np.mean((img_reference - img_compared) ** 2)
    return 10 * np.log10(d**2 / mse)


def sam(image: np.ndarray, reference: np.ndarray) -> float:
    numerator = np.sum(image * reference, axis=2)
    denominator = np.sqrt(np.sum(image ** 2, axis=2) * np.sum(reference ** 2, axis=2))
    sam_map = np.zeros_like(denominator)
    condition = denominator > 0
    sam_map[condition] = np.arccos(numerator[condition]/denominator[condition])
    return float(np.mean(sam_map)) * 180 / np.pi


def ssim(image, reference, d=255) -> float:
    out = np.empty(image.shape[2])
    for ii in np.arange(image.shape[2]):
        out[ii] = structural_similarity(image[:, :, ii], reference[:, :, ii], data_range=d)
    return float(np.mean(out))


@dataclasses.dataclass
class Results:
    student_group: str
    filelist_input: list[str]
    filelist_output: list[str]
    metrics_list: list[str]
    time_file: str
    _metric_dataframe: pd.DataFrame = None

    def metrics(self) -> pd.DataFrame:
        if self._metric_dataframe is not None:
            return self._metric_dataframe
        df_init = np.empty((len(self.filelist_input), len(self.metrics_list)))
        df = pd.DataFrame(df_init, columns=self.metrics_list)
        for ii, (img_in, img_out) in enumerate(zip(self.filelist_input, self.filelist_output)):
            reference = np.array(load_image(img_in), dtype=np.float_)
            image = np.array(load_image(img_out), dtype=np.float_)
            for metric in self.metrics_list:
                if metric == "sam":
                    df.at[ii, metric] = sam(image, reference)
                elif metric == "psnr":
                    df.at[ii, metric] = psnr(image, reference, d=255)
                elif metric == "ssim":
                    df.at[ii, metric] = ssim(image, reference, d=255)
                elif metric == "time":
                    time_array = np.load(self.time_file)
                    df.at[ii, metric] = time_array[ii]
        object.__setattr__(self, "_metric_dataframe", df)
        return df
