from __future__ import annotations
from pathlib import Path
import time
import json
import dataclasses

import numpy as np
import pandas as pd
from tqdm import tqdm

from src.validation import Results


@dataclasses.dataclass
class StudentGroup:
    label: str
    students: list[str]
    is_launched: bool


def main():

    n_patches = 50000
    flag_test = False

    students_json = "data/metadata/students.json"
    image_subfolder = "data/images"
    output_subfolder = "data/output"
    patches_path = "data/patches/patches.npy"
    evaluation_path = "data/metadata/evaluation.csv"

    project_path = Path(__file__).resolve().parents[1]
    image_folder = project_path / image_subfolder
    output_folder = project_path / output_subfolder
    file_paths = [f"{file_path.absolute()}" for file_path in image_folder.iterdir() if file_path.is_file()]
    file_paths.sort()
    patches_file = f"{project_path / patches_path}"
    students_file = f"{project_path / students_json}"

    with open(students_file) as students_file_opened:
        students_metadata = json.load(students_file_opened)
        students_list = []
        for group in students_metadata["groups"]:
            students_list.append(StudentGroup(**group))

    results = []
    for student in students_list:
        output_student = output_folder / student.label
        output_student.mkdir(exist_ok=True)
        reconstructed_list = [f"{output_student / Path(path).name}" for path in file_paths]
        student_metrics = Results(
            student_group=student.label,
            filelist_input=file_paths,
            filelist_output=reconstructed_list,
            metrics_list=["sam", "psnr", "ssim", "time"],
            time_file=f"{output_student / 'time.npy'}",
        )
        results.append(student_metrics)

    for student, result in zip(students_list, results):
        package = "reports." + student.label
        name = "run_reconstruction"
        reconstruction_function = getattr(__import__(package, fromlist=[name]), name)

        if student.is_launched and flag_test:
            time_output = np.empty(len(file_paths))
            for ii in range(len(file_paths)):
                print(f"Group: {student.label}. Image: {ii+1}/{len(file_paths)}.")
                start_time = time.time()
                reconstruction_function(
                    image_path=file_paths[ii],
                    patches_path=patches_file,
                    output_path=result.filelist_output[ii],
                    n_patches=n_patches,
                )
                time_output[ii] = time.time() - start_time
            np.save(result.file_time, time_output)

    df_students = pd.DataFrame(index=range(len(results)), columns=["group", *results[0].metrics_list])
    for ii in tqdm(np.arange(len(results))):
        df_result = results[ii].metrics()
        df_students.at[ii, "group"] = results[ii].student_group
        for metric in results[ii].metrics_list:
            mean = df_result[metric].mean()
            std = df_result[metric].std()
            mean_exponent = int(np.floor(np.log10(abs(mean))))
            mean_adjusted = mean / 10 ** mean_exponent
            std_adjusted = std / 10 ** mean_exponent
            formatted_output = f"({mean_adjusted:.2f} ± {std_adjusted:.2f}) x 10^{mean_exponent}"
            df_students.at[ii, metric] = formatted_output
    df_students.to_csv(f"{project_path / 'data/metadata/results.csv'}")
    df_students.to_markdown(f"{project_path / 'data/metadata/results.md'}", index=False)

    df_eval = pd.read_csv(project_path / evaluation_path)
    df_eval.to_markdown(project_path / "data/metadata/evaluation.md", index=False)

    df_groups = pd.DataFrame(index=range(len(students_list)), columns=["group", "members"])
    for ii, student in enumerate(students_list):
        df_groups.at[ii, "group"] = student.label
        members = ""
        for member in student.students:
            members = members + member + ", "
        members = members[:-2]
        df_groups.at[ii, "members"] = members
    df_groups.to_markdown(project_path / "data/metadata/groups.md", index=False)


if __name__ == "__main__":
    main()
