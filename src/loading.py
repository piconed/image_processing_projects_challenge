import skimage
import numpy as np


def load_image(file_path: str) -> np.ndarray:
    """Loads the image located in 'file_path'.

    Args:
        file_path (str): The path of the file containing the image. Must end by '.png'.

    Returns:
        np.ndarray: The loaded image.
    """

    if not file_path.endswith('.png'):
        raise Exception(f'file_path must end with ".png" but got "{file_path[-4:]}"')

    return skimage.io.imread(file_path)


def load_patches(patches_path: str, length: int = None) -> np.ndarray:
    """Returns into a list of size 'lenght' patches loaded from the npy file located at 'patches_path'.
    The order of the returned patches does not change.

    Args:
        patches_path (str): The path of the file containing the patches. Must end be a npy file.
        length (int): The number of the patches to load.

    Returns:
        np.ndarray: The list containing the patches.
    """

    patches = np.load(patches_path)
    length = len(patches) if length is None else length
    return patches[:length]


def save_image(image: np.ndarray, file_path: str) -> None:
    """Saves the image in 'file_path'. The image is saved in the png format.

    Args:
        image (np.ndarray): The image to be saved.
        file_path (str): The path of the file in which the image will be saved. If a file already exists it is replaced. It must end by '.png'.
    """

    if not file_path.endswith('.png'):
        raise Exception(f'file_path must end with ".png" but got "{file_path[-4:]}"')

    skimage.io.imsave(file_path, image.astype('uint8'))


####
####
####

####      ####                ####        #############
####      ######              ####      ##################
####      ########            ####      ####################
####      ##########          ####      ####        ########
####      ############        ####      ####            ####
####      ####  ########      ####      ####            ####
####      ####    ########    ####      ####            ####
####      ####      ########  ####      ####            ####
####      ####  ##    ######  ####      ####          ######
####      ####  ####      ##  ####      ####    ############
####      ####  ######        ####      ####    ##########
####      ####  ##########    ####      ####    ########
####      ####      ########  ####      ####
####      ####        ############      ####
####      ####          ##########      ####
####      ####            ########      ####
####      ####              ######      ####

# 2023
# Authors : Matthieu Chancel, Mauro Dalla Mura, Matthieu Muller and Daniele Picone