| group     | members                               |
|:----------|:--------------------------------------|
| agoumbi   | Agoumbi, Atanda, Bah, Diabate         |
| augereau  | Augereau, Charmot                     |
| baud      | Baud, Chêne, Chareyron                |
| benedetti | Benedetti, Runel, Soufargi            |
| combat    | Combat, Jouannin, Lacroix             |
| doussain  | Doussain, El Gaf, Haudrechy, Masselis |
| dupre     | Dupré, Laurencin, Machado             |
| gailly    | Gailly, Guefif, Hanol                 |
| mjaidila  | Mjaidila, Manzini, Shahin, Torres     |
| novarina  | Novarina, Rocher, Serasse, Tognellini |
| owona     | Owona, Ramone, Verstraeten            |
| pelops    | Pelops, Turki                         |