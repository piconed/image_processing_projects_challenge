| group     | signature   | global   | segmented   | debug   | extension   | unique-patch   |
|:----------|:------------|:---------|:------------|:--------|:------------|:---------------|
| agoumbi   | x           | v        | x           | v       | v           | x              |
| augereau  | x           | v        | x           | v       | v           | v              |
| baud      | x           | v        | x           | v       | v           | v              |
| benedetti | x           | x        | x           | v       | v           | v              |
| combat    | x           | v        | v           | v       | x           | v              |
| dupre     | x           | v        | x           | v       | x           | v              |
| doussain  | v           | v        | v           | v       | x           | v              |
| gailly    | x           | x        | v           | v       | v           | v              |
| mjaidila  | v           | v        | v           | x       | x           | v              |
| novarina  | x           | v        | v           | v       | v           | v              |
| owona     | x           | v        | x           | v       | v           | x              |
| pelops    | v           | v        | v           | v       | v           | x              |